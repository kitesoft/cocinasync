unit promises.test;

interface

procedure TestPromises;

var
  Terminated : boolean;

implementation

uses System.SysUtils, System.Classes, cocinasync.promise, cocinasync.async;

procedure TestPromises;
var
  sl : TStringList;
begin
  sl := TStringList.Create;
  sl.Add('start');
  Promise(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 20;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('A1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&And(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('A2 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
      //raise Exception.Create('Hey you');
    end
  ).&Then(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 10;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).All([
    procedure
    var
      iVal : Integer;
    begin
      iVal := 20;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B2a : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end,
    procedure
    var
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('B2b : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ]).&Then(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 100;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('C1 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&And(
    procedure
    var
      iVal : Integer;
    begin
      iVal := 200;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('C2 : '+iVal.ToString);
      finally
        TMonitor.Exit(sl);
      end;
    end
  ).&Then(
    procedure
    var
      s : string;
      iVal : Integer;
    begin
      iVal := 0;
      sleep(iVal);
      TMonitor.Enter(sl);
      try
        sl.Add('D1 : '+iVal.ToString);
        s := sl.Text;
      finally
        TMonitor.Exit(sl);
      end;
      TAsync.SynchronizeIfInThread(
        procedure
        begin
          WriteLn(s);
        end
      );
      sl.Free;
      Terminated := True;
    end
  ).OnError(
    procedure(E : Exception; var Abort : boolean)
    begin
      TMonitor.Enter(sl);
      try
        sl.Add('Exception encountered: '+E.Message);
      finally
        TMonitor.Exit(sl);
      end;
      Abort := False;
    end
  ).Done;
  TMonitor.Enter(sl);
  try
    sl.Add('exiting');
  finally
    TMonitor.Exit(sl);
  end;

end;

initialization
  Terminated := False;
  Randomize;

end.
