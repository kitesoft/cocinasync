program flux_vcl_demo;

uses
  Vcl.Forms,
  fluxdemo.vcl.main in 'fluxdemo.vcl.main.pas' {frmToDoList},
  fluxdemo.stores.item in 'fluxdemo.stores.item.pas' {dmItemStore: TDataModule},
  fluxdemo.actions.todoitem in 'fluxdemo.actions.todoitem.pas',
  fluxdemo.vcl.history in 'fluxdemo.vcl.history.pas' {frmHistory},
  fluxdemo.stores.history in 'fluxdemo.stores.history.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdmItemStore, dmItemStore);
  Application.CreateForm(TfrmToDoList, frmToDoList);
  Application.CreateForm(TfrmHistory, frmHistory);
  Application.Run;
end.
