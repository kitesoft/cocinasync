unit fluxdemo.vcl.history;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TfrmHistory = class(TForm)
    lbHistory: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHistory: TfrmHistory;

implementation

uses fluxdemo.stores.history;

{$R *.dfm}

procedure TfrmHistory.FormCreate(Sender: TObject);
begin
  HistoryStore.RegisterForUpdates<THistoryStore>(Self,
    procedure(Store : THistoryStore)
    begin
      lbHistory.Items.BeginUpdate;
      try
        lbHistory.Items.Assign(Store.Items);
      finally
        lbHistory.Items.EndUpdate;
      end;
    end
  );
end;

procedure TfrmHistory.FormDestroy(Sender: TObject);
begin
  HistoryStore.UnregisterForUpdates(Self);
end;

end.
