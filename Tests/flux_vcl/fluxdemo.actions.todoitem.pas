unit fluxdemo.actions.todoitem;

interface

uses System.SysUtils, System.Classes, cocinasync.flux.action;

type
  TNewItemAction = class(TBaseAction)
  private
    FText: string;
    FID: TGUID;
  public
    function ToString: string; override;
    property Text : string read FText;
    property ID : TGUID read FID;
    class function New(ID : TGuid; Text : string) : TNewItemAction;
  end;

  TDeleteItemAction = class(TBaseAction)
  private
    FID: TGUID;
  public
    function ToString: string; override;
    property ID : TGUID read FID;
    class function New(ID : TGuid) : TDeleteItemAction;
  end;

  TToggleItemAction = class(TBaseAction)
  private
    FID: TGUID;
    FChecked: Boolean;
  public
    function ToString: string; override;
    property ID : TGUID read FID;
    property Checked : Boolean read FChecked;
    class function New(ID : TGuid; Checked : boolean) : TToggleItemAction;
  end;

implementation

{ TNewItemAction }

class function TNewItemAction.New(ID: TGuid; Text: string): TNewItemAction;
begin
  Result := inherited New<TNewItemAction>;
  Result.FID := ID;
  Result.FText := Text;
end;

function TNewItemAction.ToString: string;
begin
  Result := 'Added Item ('+GuidToString(FID)+': '+FText+')';
end;

{ TDeleteItemAction }

class function TDeleteItemAction.New(ID: TGuid): TDeleteItemAction;
begin
  Result := inherited New<TDeleteItemAction>;
  Result.FID := ID;
end;

function TDeleteItemAction.ToString: string;
begin
  Result := 'Deleted Item ('+GuidToString(FID)+')';
end;

{ TToggleItemAction }

class function TToggleItemAction.New(ID: TGuid;
  Checked: boolean): TToggleItemAction;
begin
  Result := inherited New<TToggleItemAction>;
  Result.FID := ID;
  Result.FChecked := Checked;
end;

function TToggleItemAction.ToString: string;
begin
  if FChecked then
    Result := 'Marked Finished ('+GuidToString(FID)+')'
  else
    Result := 'Marked Unfinished ('+GuidToString(FID)+')';
end;

end.
