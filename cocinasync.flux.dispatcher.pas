unit cocinasync.flux.dispatcher;

interface

uses System.SysUtils, System.Classes, cocinasync.flux.action, System.Generics.Collections,
  cocinasync.flux.store;

type
  TFluxDispatcher = class(TObject)
  strict private
    type
      TStoreRegistration<AC : TBaseAction> = class(TObject)
        Store : TBaseStore;
        ClassType : TClass;
        Handler : TProc<AC>;
        type
          PAC = ^AC;
        constructor Create(AStore : TBaseStore; AHandler : TProc<AC>);
        function AsBase : TStoreRegistration<TBaseAction>;
      end;
  strict private
    FDispatchDepth : Integer;
    FCS : TMREWSync;
    FNotifications : TDictionary<TActionClass, TList<TStoreRegistration<TBaseAction>>>;
  private
    procedure EnsureActionExistsForNotifications(Action: TActionClass);
  public
    procedure Register<AC : TBaseAction>(Store : TBaseStore; Handler : TProc<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TBaseStore; Event : TActionEvent<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TBaseDataModuleStore; Handler : TProc<AC>); overload;
    procedure Register<AC : TBaseAction>(Store : TBaseDataModuleStore; Event : TActionEvent<AC>); overload;
    procedure Unregister<AC : TBaseAction>(Store : TBaseStore); overload;
    procedure Unregister<AC : TBaseAction>(Store : TBaseDataModuleStore); overload;
    procedure WaitFor(StoreIDs : TArray<TStoreID>; ThenDo : TProc);
    procedure Dispatch(Action : TBaseAction);
    function IsDispatching : Boolean;

    constructor Create;
    destructor Destroy; override;

  end;

var
  Flux : TFluxDispatcher;

implementation

uses System.SyncObjs;

{ TFluxDispatcher }

constructor TFluxDispatcher.Create;
begin
  inherited Create;
  FNotifications := TDictionary<TActionClass, TList<TStoreRegistration<TBaseAction>>>.Create;
  FDispatchDepth := 0;
  FCS := TMREWSync.Create;
end;

destructor TFluxDispatcher.Destroy;
var
  ary : TArray<TPair<TActionClass, TList<TStoreRegistration<TBaseAction>>>>;
  p : TPair<TActionClass, TList<TStoreRegistration<TBaseAction>>>;
begin
  FCS.BeginWrite;
  try
    ary := FNotifications.ToArray;
    for p in ary do
    begin
      while p.Value.Count > 0 do
        p.Value.ExtractAt(0).Free;
      p.Value.Free;
    end;
    FNotifications.Free;
  finally
    FCS.EndWrite;
  end;
  FCS.Free;
  inherited;
end;

procedure TFluxDispatcher.EnsureActionExistsForNotifications(Action: TActionClass);
begin
  FCS.BeginRead;
  try
    if not FNotifications.ContainsKey(Action) then
    begin
      FCS.BeginWrite;
      try
        FNotifications.Add(Action, TList<TStoreRegistration<TBaseAction>>.Create);
      finally
        FCS.EndWrite;
      end;
    end;
  finally
    FCS.EndRead;
  end;
end;

procedure TFluxDispatcher.Dispatch(Action: TBaseAction);
var
  lst : TList<TStoreRegistration<TBaseAction>>;
  sr : TStoreRegistration<TBaseAction>;
  item : TStoreRegistration<TBaseAction>;
begin
  FCS.BeginRead;
  try
    if FNotifications.ContainsKey(TActionClass(Action.ClassType)) then
    begin
      TInterlocked.Increment(FDispatchDepth);
      try
        lst := FNotifications[TActionClass(Action.ClassType)];
        TMonitor.Enter(lst);
        try
          for item in lst do
          begin
            if Assigned(item.Handler) then
              item.Handler(Action)
          end;
        finally
          TMonitor.Exit(lst);
        end;
      finally
        TInterlocked.Decrement(FDispatchDepth);
      end;
    end;
  finally
    FCS.EndRead;
  end;
end;

function TFluxDispatcher.IsDispatching: Boolean;
begin
  Result := FDispatchDepth = 0;
end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseStore; Handler: TProc<AC>);
var
  lst : TList<TStoreRegistration<TBaseAction>>;
begin
  FCS.BeginRead;
  try
    EnsureActionExistsForNotifications(AC);
    lst := FNotifications[AC];
    TMonitor.Enter(lst);
    try
      lst.Add(TStoreRegistration<AC>.Create(Store,
        procedure(Action : AC)
        begin
          Handler(Action);
          Store.UpdateViews;
        end
      ).AsBase);
    finally
      TMonitor.Exit(lst);
    end;
  finally
    FCS.EndRead;
  end;
end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseStore; Event: TActionEvent<AC>);
var
  lst : TList<TStoreRegistration<TBaseAction>>;
begin
  FCS.BeginRead;
  try
    EnsureActionExistsForNotifications(AC);
    lst := FNotifications[AC];
    TMonitor.Enter(lst);
    try
      lst.Add(TStoreRegistration<AC>.Create(Store,
        procedure(Action : AC)
        begin
          Event(Store, Action);
          Store.UpdateViews;
        end
      ).AsBase);
    finally
      TMonitor.Exit(lst);
    end;
  finally
    FCS.EndRead;
  end;
end;

procedure TFluxDispatcher.Unregister<AC>(Store: TBaseStore);
var
  item : TStoreRegistration<TBaseAction>;
begin
  FCS.BeginRead;
  try
    if not FNotifications.ContainsKey(AC) then
      exit;
    for item in FNotifications[AC] do
    begin
      if item.Store = Store then
      begin
        FCS.BeginWrite;
        try
          FNotifications[AC].Remove(item);
          item.Free;
        finally
          FCS.EndWrite;
        end;
        break;
      end;
    end;
  finally
    FCS.EndRead;
  end;
end;

procedure TFluxDispatcher.WaitFor(StoreIDs: TArray<TStoreID>; ThenDo: TProc);
begin

end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseDataModuleStore; Event: TActionEvent<AC>);
begin
  Register<AC>(Store.BaseStore, Event);
end;

procedure TFluxDispatcher.Unregister<AC>(Store: TBaseDataModuleStore);
begin
  Unregister<AC>(Store.BaseStore);
end;

procedure TFluxDispatcher.Register<AC>(Store: TBaseDataModuleStore; Handler: TProc<AC>);
begin
  Register<AC>(Store.BaseStore, Handler);
end;

{ TFluxDispatcher.TStoreRegistration }

function TFluxDispatcher.TStoreRegistration<AC>.AsBase: TStoreRegistration<TBaseAction>;
begin
  Result := TStoreRegistration<TBaseAction>(Self);
end;

constructor TFluxDispatcher.TStoreRegistration<AC>.Create(AStore : TBaseStore; AHandler : TProc<AC>);
begin
  inherited Create;
  Store := AStore;
  ClassType := AC;
  Handler := AHandler;
end;

initialization
  Flux := TFluxDispatcher.Create;


finalization
  Flux.Free;

end.
