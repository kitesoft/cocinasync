unit cocinasync.flux.action;

interface

uses System.SysUtils, System.Classes, System.Generics.Collections;

type
  TBaseAction = class;

  TActionClass = class of TBaseAction;

  TBaseAction = class(TObject)
  strict private
    class var FCS : TMREWSync;
    class var FActionPool : TDictionary<TClass, TQueue<TBaseAction>>;
  protected
    class function New<AC : TBaseAction, constructor> : AC;
    class procedure Finish(Action : TBaseAction);
    class procedure ClearPools;
  public
    constructor Create;

    class constructor Create;
    class destructor Destroy;
  end;

  TActionEvent<AC : TBaseAction> = procedure(Sender : TObject; Action : AC);


implementation

{ TBaseAction }

class procedure TBaseAction.ClearPools;
var
  ary : TArray<TPair<TClass, TQueue<TBaseAction>>>;
  p: TPair<TClass, TQueue<TBaseAction>>;
begin
  FCS.BeginWrite;
  try
    ary := FActionPool.ToArray;
    for p in ary do
    begin
      TMonitor.Enter(p.Value);
      try
        while p.Value.Count > 0 do
          p.Value.Dequeue.Free;
      finally
        TMonitor.Exit(p.Value);
      end;
    end;
  finally
    FCS.EndWrite;
  end;
end;

class constructor TBaseAction.Create;
begin
  FCS := TMREWSync.Create;
  FActionPool := TDictionary<TClass, TQueue<TBaseAction>>.Create;
end;

constructor TBaseAction.Create;
begin
  inherited Create;
end;

class destructor TBaseAction.Destroy;
var
  ary : TArray<TPair<TClass, TQueue<TBaseAction>>>;
  p: TPair<TClass, TQueue<TBaseAction>>;
begin
  FCS.BeginWrite;
  try
    ary := FActionPool.ToArray;
    for p in ary do
    begin
      TMonitor.Enter(p.Value);
      try
        while p.Value.Count > 0 do
          p.Value.Dequeue.Free;
      finally
        TMonitor.Exit(p.Value);
      end;
      p.Value.Free;
    end;
  finally
    FCS.EndWrite;
  end;
  FCS.Free;
  FActionPool.Free;
end;

class function TBaseAction.New<AC>: AC;
var
  queue : TQueue<TBaseAction>;
begin
  Result := nil;
  FCS.BeginRead;
  try
    if FActionPool.ContainsKey(AC) then
    begin
      queue := FActionPool[AC];
      TMonitor.Enter(queue);
      try
        if queue.Count > 0 then
          Result := AC(queue.Dequeue)
      finally
        TMonitor.Exit(queue);
      end;
    end;
  finally
    FCS.EndRead;
  end;

  if Result = nil then
    Result := AC.Create;

end;

class procedure TBaseAction.Finish(Action: TBaseAction);
var
  queue : TQueue<TBaseAction>;
begin
  FCS.BeginRead;
  try
    if FActionPool.ContainsKey(Action.ClassType) then
    begin
      queue := FActionPool[Action.ClassType];
      TMonitor.Enter(queue);
      try
        queue.Enqueue(Action);
      finally
        TMonitor.Exit(queue);
      end;
    end else
    begin
      FCS.BeginWrite;
      try
        if FActionPool.ContainsKey(Action.ClassType) then
        begin
          Finish(Action);
          exit;
        end;
        queue := TQueue<TBaseAction>.Create;
        queue.Enqueue(Action);
        FActionPool.Add(Action.ClassType, queue);
      finally
        FCS.EndWrite;
      end;
    end;
  finally
    FCS.EndRead;
  end;

end;

end.
