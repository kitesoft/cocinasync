unit cocinasync.promise;

interface

uses
  System.SysUtils;

type
  TAbortableExceptionHandler = reference to procedure(E : Exception; var Abort : boolean);
  TExceptionHandler = reference to procedure(E : Exception);

  IPromise = interface
    function All(const ary : TArray<TProc>) : IPromise;
    function Resolve(Proc : TProc) : IPromise;
    function &And(Proc : TProc) : IPromise;
    function &Then(Proc : TProc) : IPromise;
    function OnError(Proc : TAbortableExceptionHandler) : IPromise; overload;
    function OnError(Proc : TExceptionHandler) : IPromise; overload;
    procedure Done;
  end;

  function Promise(Proc : TProc) : IPromise;

implementation

uses
  System.SyncObjs,
  System.Generics.Collections,
  System.Classes,
  Cocinasync.collections,
  Cocinasync.jobs;


type
  TProc = System.SysUtils.TProc;
  TPromise = class(TInterfacedObject, IPromise)
    type
      TPromiseThread = class(TThread)
      private
        FCount : integer;
        FCurrentJobs : TQueue<TProc>;
        FDone : Boolean;
        FNextJobs : TQueue<TProc>;
        FQueues : TQueue<TQueue<TProc>>;
        FPromise : IPromise;
        FOnError: TAbortableExceptionHandler;
      protected
        procedure TerminateThread;
        procedure Execute; override;
      public
        procedure AddJob(const Proc : TProc; Wait : boolean);
        property OnError : TAbortableExceptionHandler read FOnError write FOnError;
        constructor Create(Promise : IPromise);
        destructor Destroy; override;
      end;
  private
    FThread : TPromiseThread;
  public
    function All(const ary : TArray<TProc>) : IPromise;
    function Resolve(Proc : TProc) : IPromise;
    function &And(Proc : TProc) : IPromise;
    function &Then(Proc : TProc) : IPromise;
    function OnError(Proc : TAbortableExceptionHandler) : IPromise; overload;
    function OnError(Proc : TExceptionHandler) : IPromise; overload;
    procedure Done;

    constructor Create;
    destructor Destroy; override;
  end;

function Promise(Proc : TProc) : IPromise; overload;
begin
  Result := TPromise.Create;
  Result.Resolve(Proc);
end;

{ TPromise }

constructor TPromise.Create;
begin
  inherited Create;
  FThread := TPromiseThread.Create(Self);
end;

destructor TPromise.Destroy;
begin
  FThread.Terminate;
  inherited;
end;

procedure TPromise.Done;
begin
  FThread.FDone := True;
end;

function TPromise.OnError(Proc : TExceptionHandler): IPromise;
begin
  Result := Self;
  FThread.OnError :=
    procedure(E : Exception; var Abort : boolean)
    begin
      Proc(E);
      Abort := True;
    end;
end;

function TPromise.OnError(Proc : TAbortableExceptionHandler): IPromise;
begin
  Result := Self;
  FThread.OnError := Proc;
end;

function TPromise.&And(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, False);
  Result := Self;
end;

function TPromise.All(const ary: TArray<TProc>): IPromise;
var
  p : TProc;
begin
  for p in ary do
  begin
    FThread.AddJob(p, False);
  end;
  result := Self;
end;

function TPromise.Resolve(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, False);
  Result := Self;
end;

function TPromise.&Then(Proc: TProc): IPromise;
begin
  FThread.AddJob(Proc, True);
  Result := Self;
end;

{ TPromise.TPromiseThread }

procedure TPromise.TPromiseThread.Execute;
var
  p : TProc;
begin
  repeat
    if Terminated then
      exit
    else if not FDone then
    begin
      sleep(10);
      continue;
    end;

    if FCurrentJobs.Count > 0 then
    begin
      TInterlocked.Increment(FCount);
      p := FCurrentJobs.Dequeue();
      TJobManager.Execute(
        procedure
        var
          bAbort : boolean;
        begin
          try
            try
              p();
            except
              on e: exception do
                if Assigned(FOnError) then
                begin
                  bAbort := True;
                  FOnError(E, bAbort);
                  if bAbort then
                  begin
                    TerminateThread;
                  end;
                end;
            end;
          finally
            TInterlocked.Decrement(FCount);
          end;
        end
      );
    end else
    begin
      if (FCount = 0) and (FQueues.Count > 0) then
      begin
        TMonitor.Enter(FQueues);
        try
          FCurrentJobs.Free;
          FCurrentJobs := FQueues.Dequeue;
        finally
          TMonitor.Exit(FQueues);
        end;
      end;
      if (FCount = 0) and (FQueues.Count = 0) and (FCurrentJobs.Count = 0) then
        Terminate;
    end;

    sleep(1);
  until Terminated;
  FPromise := nil;
end;

procedure TPromise.TPromiseThread.TerminateThread;
begin
  Terminate;
end;

procedure TPromise.TPromiseThread.AddJob(const Proc: TProc; Wait : boolean);
begin
  if Wait then
  begin
    TMonitor.Enter(FQueues);
    try
      FNextJobs := TQueue<TProc>.Create(1024);
      FQueues.Enqueue(FNextJobs);
      FNextJobs.Enqueue(Proc);
    finally
      TMonitor.Exit(FQueues);
    end;
  end else
    FNextJobs.Enqueue(Proc)
end;

constructor TPromise.TPromiseThread.Create(Promise : IPromise);
begin
  inherited Create(False);
  FCount := 0;
  FDone := False;
  FPromise := Promise;
  FreeOnTerminate := True;
  FQueues := TQueue<TQueue<TProc>>.Create(1024);
  FCurrentJobs := TQueue<TProc>.Create(1024);
  FNextJobs := FCurrentJobs;
  FOnError := nil;
end;

destructor TPromise.TPromiseThread.Destroy;
var
  q : TQueue<TProc>;
begin
  repeat
    q := FQueues.Dequeue;
    if Assigned(q) then
      q.Free;
  until not Assigned(q);
  FCurrentJobs.Free;
  FQueues.Free;
  inherited;
end;

end.
